import java.io.*;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.Semaphore;
import java.nio.file.Files;

public class p2pPeerThread extends Thread {
	protected DatagramSocket socket = null;
	protected DatagramPacket packet = null;
	protected InetAddress addr = null;
	protected byte[] resource = new byte[1024];
	protected byte[] response = new byte[1024];
	protected int port;
	protected String[] vars;

	public p2pPeerThread(String ip, String nome) throws Exception {
		final File folder = new File("./Files");
		List<resources> lista = listFilesForFolder(folder);
		String teste = "create "+ nome +" "+ resources.listaParaTexto(lista);
		resource = teste.getBytes();
		addr = InetAddress.getByName(ip);
		port = Integer.parseInt("9001");
		// cria um socket datagrama
		socket = new DatagramSocket(port);
	}

	static class comunicaOnline implements Runnable {
		Semaphore mutex;
		Semaphore [] s;
		private int tid;
		InetAddress addr;
		DatagramSocket socket = null;

		public comunicaOnline(int id, Semaphore mtx, Semaphore [] sem, InetAddress enderecoServidor){
			this.tid = id;
			mutex = mtx;
			s = sem;
			addr = enderecoServidor;
		}

		public void run(){
			try {
				socket = new DatagramSocket(9010);
			}catch (SocketException e){
				e.printStackTrace();
			}

			while (true) {
				try {
					Thread.sleep(10000);
					byte[] message = "Pai ta On!".getBytes();
					DatagramPacket packet = new DatagramPacket(message, message.length, addr, 9000);
					socket.send(packet);
				} catch (InterruptedException | IOException e) {
					e.printStackTrace();
				}
			}
		}

	}
	static class recebeComandos implements Runnable {
		Semaphore mutex;
		Semaphore [] s;
		private int tid;
		protected String content = null;
		protected InetAddress addr;
		protected int port;
		protected byte[] resource = new byte[1024];
		protected byte[] response = new byte[1024];
		protected DatagramSocket socket = new DatagramSocket(9002);
		protected DatagramPacket packet;

		public recebeComandos(int id, Semaphore mtx, Semaphore [] sem) throws IOException {
			this.tid = id;
			mutex = mtx;
			s = sem;
		}

		public void run(){
			try {
				while(true) {
					// recebe datagrama
					packet = new DatagramPacket(resource, resource.length);
					socket.receive(packet);

					// processa o que foi recebido, adicionando a uma lista
					content = new String(packet.getData(), 0, packet.getLength());
					addr = packet.getAddress();
					port = packet.getPort();
					String vars[] = content.split("\\s");

					String data = "";

					//Cada comando
					if (vars[0].equals("download") && vars.length > 1) {
						String arquivo = vars[1];

						Path enderecoArquivo = Paths.get("Files/" + arquivo);
						if (Files.exists(enderecoArquivo)) {
							String conteudoArquivo = Files.readString(enderecoArquivo);
							String conteudo = "newfile" + ":" + arquivo + ":" + conteudoArquivo;

							ByteArrayOutputStream arquivoEnvio = new ByteArrayOutputStream(conteudoArquivo.length());
							arquivoEnvio.write(conteudo.getBytes());
							arquivoEnvio.flush();
							arquivoEnvio.close();

							DatagramPacket filePacket = new DatagramPacket(arquivoEnvio.toByteArray(), conteudo.length(), packet.getAddress(), 9003);
							socket.send(filePacket);
							data += "Arquivo sendo enviado!";
							response = data.getBytes();
							packet = new DatagramPacket(response, response.length, addr, port);
							socket.send(packet);
						} else {
							data += "Arquivo não encontrado!";
							response = data.getBytes();
							packet = new DatagramPacket(response, response.length, addr, port);
							socket.send(packet);
						}
					} else {
						data += "Comando não aceito pelo peer!";
						response = data.getBytes();
						packet = new DatagramPacket(response, response.length, addr, port);
						socket.send(packet);
					}
				}
			}catch (IOException e) {
			}
		}

	}

	static class recebeArquivos implements Runnable {
		Semaphore mutex;
		Semaphore [] s;
		private int tid;
		protected byte[] resource = new byte[1024];
		protected DatagramSocket socket = new DatagramSocket(9003);
		protected DatagramPacket packet;

		public recebeArquivos(int id, Semaphore mtx, Semaphore [] sem) throws IOException {
			this.tid = id;
			mutex = mtx;
			s = sem;
		}

		public void run(){
			try {
				while(true) {
					// recebe datagrama
					packet = new DatagramPacket(resource, resource.length);
					socket.receive(packet);

					// processa o que foi recebido, adicionando na pasta de download
					String resposta = new String(packet.getData(), 0, packet.getLength());

					if (resposta.length() > 0) {
						String[] vars = resposta.split(":");

						if (vars[0].equals("newfile")) {
							File newFile = new File("Downloads/copy_" + vars[1]);
							FileOutputStream fos = new FileOutputStream(newFile);
							if (vars.length > 2) {
								fos.write(vars[2].getBytes(StandardCharsets.UTF_8), 0, vars[2].length());
							}
							fos.flush();
							fos.close();
						}
					}
				}
			}catch (IOException e) {
				e.printStackTrace();
			}
		}

	}

	public List<resources> listFilesForFolder(final File folder) throws Exception {
		List<resources> lista = new ArrayList<>();
		for (final File fileEntry : folder.listFiles()) {
			if (fileEntry.isDirectory()) {
				listFilesForFolder(fileEntry);
			} else {
				String fileName = fileEntry.getName();
				String hash = hashGenerator.generate(fileEntry);
				lista.add(new resources(fileName,hash));
			}
		}
		return lista;
	}

	public void run() {
		try {
			// envia um packet criando o usuario
			DatagramPacket packet = new DatagramPacket(resource, resource.length, addr, 9000);
			socket.send(packet);

			Semaphore mutex = new Semaphore(1);
			Semaphore [] s = new Semaphore [3];

			// Cria as threads
			Thread t1 = new Thread(new comunicaOnline(1, mutex, s, addr));
			t1.start();
			Thread t2 = new Thread(new recebeComandos(2, mutex, s));
			t2.start();
			Thread t3 = new Thread(new recebeArquivos(3, mutex, s));
			t3.start();

			while(true){
				// obtem a resposta
				packet = new DatagramPacket(response, response.length);
//				socket.setSoTimeout(5000);
				socket.receive(packet);

				// mostra a resposta
				String data = new String(packet.getData(), 0, packet.getLength());
				System.out.println("recebido: " + data);

				Scanner scan = new Scanner(System.in);
				System.out.println("Comandos disponiveis:");
				System.out.println("buscar <texto>");
				System.out.println("download <nomeArquivo> <ip>");
				String comando = scan.nextLine();
				String[] vars = comando.split("\\s");
				if(vars[0].equals("download") && vars.length > 2){
					byte[] captura = (vars[0]+" "+vars[1]).getBytes();
					DatagramPacket pacote = new DatagramPacket(captura, captura.length, InetAddress.getByName(vars[2]), 9002);
					socket.send(pacote);
				}else {
					byte[] captura = comando.getBytes();
					DatagramPacket pacote = new DatagramPacket(captura, captura.length, addr, 9000);
					socket.send(pacote);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
