import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

public class usuario {
    private String userName = null;
    private InetAddress resourceAdr = null;
    private Integer port = null;
    private List<resources> fileList = new ArrayList<>();

    public usuario(String userName, InetAddress addr, Integer port, List<resources> fileList){
        this.userName = userName;
        this.resourceAdr = addr;
        this.port = port;
        this.fileList = fileList;
    }

    public String getUserName(){
        return userName;
    }

    public String getAddress(){
        return resourceAdr.toString();
    }

    public List<resources> getFileList(){
        return fileList;
    }

    public String toString() {
        return "Username: " + userName + " IP: " + resourceAdr.toString() + " Porta:" + port;
    }
}
