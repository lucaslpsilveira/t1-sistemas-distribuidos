import java.io.*;
import java.net.*;
import java.util.*;

public class p2pServerThread extends Thread {
	protected String content = null;
	protected InetAddress addr;
	protected int port;
	protected byte[] resource = new byte[1024];
	protected byte[] response = new byte[1024];
	protected DatagramSocket socket = new DatagramSocket(9000);
	protected DatagramPacket packet;

	public p2pServerThread() throws IOException {
		port = Integer.parseInt("9000");
	}

	public void run() {
		List<usuario> userList = new ArrayList<>();

		while (true) {
			try {
				// recebe datagrama
				packet = new DatagramPacket(resource, resource.length);
				socket.setSoTimeout(500);
				socket.receive(packet);
								
				// processa o que foi recebido, adicionando a uma lista
				content = new String(packet.getData(), 0, packet.getLength());
				addr = packet.getAddress();
				port = packet.getPort();
				String vars[] = content.split("\\s");

				System.out.print("\n Recebi! de: " + addr.toString() +" conteudo: " + content);

				if (vars[0].equals("create") && vars.length > 1) {
					int j;

					for (j = 0; j < userList.size(); j++) {
						if (userList.get(j).equals(vars[1]))
							break;
					}

					if (j == userList.size()) {
						List<resources> lista = vars.length >2 ? resources.textoParaLista(vars[2]) : new ArrayList<>();
						userList.add(new usuario(vars[1], addr, port, lista));
						response = "OK".getBytes();
					} else {
						response = "NOT OK".getBytes();
					}

					packet = new DatagramPacket(response, response.length, addr, port);
					socket.send(packet);
				}
				
				if (vars[0].equals("buscar")) {
					String data = "";
					if (vars.length > 1){
						//Filtro por nome do arquivo
						for(int i = 0; i < userList.size(); i++){
							List<resources> lista = userList.get(i).getFileList();
							for(int j = 0; j < lista.size(); j++){
								if(lista.get(j).getFileName().contains(vars[1])){
									data += lista.get(j).getFileName()+" peer: "+userList.get(i).getAddress() + "\n";
								}
							};
						}
					}else{
						//Lista todos
						if(userList.size() > 0){
							for(int i = 0; i < userList.size(); i++){
								data += userList.get(i).toString() + "\n";
							}
						}else {
							data = "Nenhum Usuário cadastrado!";
						}
					}
					response = data.getBytes();
					packet = new DatagramPacket(response, response.length, addr, port);
					socket.send(packet);
				}

				if (!vars[0].equals("buscar") && !vars[0].equals("create")) {
					String texto = "Comando Inválido!";
					response = texto.getBytes();
					packet = new DatagramPacket(response, response.length, addr, port);
					socket.send(packet);
				}
			} catch (IOException e) {
//				System.out.print(".");
			}
		}
	}
}
