import java.util.Scanner;

public class appP2P {

    public static void main(String[] args) throws Exception {
        if (args.length != 1) {
            System.out.println("Qual o seu papel na rede?");
            System.out.println("Informe 'peer' ou 'servidor'");
            return;
        } else {
            switch(args[0]){
                case "peer":
                    Scanner s = new Scanner(System.in);
                    System.out.println("Informe o ip do servidor:");
                    String ip = s.nextLine();
                    System.out.println("Informe seu nome de usuario:");
                    String nome = s.nextLine();
                    new p2pPeerThread(ip, nome).start();
                    break;
                case "servidor":
                    new p2pServerThread().start();
                    break;
                default:
                    System.out.println("Informe um valor valido!");
                    System.out.println("'peer' ou 'servidor'");

            }
        }
    }
}