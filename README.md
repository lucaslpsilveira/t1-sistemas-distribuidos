# T1 Sistemas Distribuídos

Desenvolvido por: Lucas Silveira e Lucas Pescador


## Compilando o projeto

```
make
```

## Iniciando

Para executar como servidor:

```
java appP2P servidor
```

Para executar como peer:
```
java appP2P peer
```


## Executando como peer:

Após enviar as informações solicitadas pelo sistema, o usuário terá os seguintes comandos disponíveis:

```
buscar <texto>
```

```
download <nomeArquivo> <ip>
```

Arquivos disponíveis para testar o comando download:
<br>arq.txt
<br>pepaPig.txt
<br>teste.txt
<br>teste2.txt
