import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;

public class resources {
    private String filename = null;
    private String hash = null;

    public resources(String filename, String hash){
        this.filename = filename;
        this.hash = hash;
    }

    public String getFileName(){
        return filename;
    }

    public String toString(){
        return filename+":"+hash;
    }

    public static String listaParaTexto(List<resources> arquivos){
        final StringBuilder builder = new StringBuilder();
        arquivos.forEach(arquivo -> {
            builder.append(arquivo.toString() + ";");
        });
        return builder.toString();
    }

    public static List<resources> textoParaLista(String arquivos){
        List<resources> lista = new ArrayList<>();
        String[] vars = arquivos.split(";");
        for(int i = 0; i < vars.length; i++){
            String[] vars2 = vars[i].split(":");
            lista.add(new resources(vars2[0],vars2[1]));
        }
        return  lista;
    }
}
